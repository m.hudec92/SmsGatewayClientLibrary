/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sk.stuba.fiit.dp.xhudecm1.smsgatewayclientlibrary;

import com.google.gson.FieldNamingPolicy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import sk.stuba.fiit.dp.xhudecm1.smsgatewayclientlibrary.exceptions.UnableToWriteConfigurationFileException;
import sk.stuba.fiit.dp.xhudecm1.smsgatewayclientlibrary.jsonobjects.OptionProperties;
import sk.stuba.fiit.dp.xhudecm1.smsgatewayclientlibrary.smshandler.ClientSmsHandler;

import sk.stuba.fiit.dp.xhudecm1.smsgatewayclientlibrary.smshandler.ClientSmsHandlerInterface;
import sk.stuba.fiit.dp.xhudecm1.smsgatewaysmshandler.smshandler.exceptions.UnableToLoadModemConfiguration;
import sk.stuba.fiit.dp.xhudemc1.serialgsmmodemcommunicator.exceptions.SerialGsmModemCommunicatorException;

/**
 *
 * @author martinhudec
 */
public class SmsGatewayClient {

    private String optionPropertiesPath;
    private final ClientSmsHandlerInterface smsHandler;
    public static OptionProperties OPTION_PROPERTIES;
    private final static org.apache.log4j.Logger LOG = org.apache.log4j.Logger.getLogger(SmsGatewayClient.class);

    public SmsGatewayClient(String optionPropertiesPath) throws UnableToWriteConfigurationFileException, SerialGsmModemCommunicatorException, UnableToLoadModemConfiguration  {
        this.optionPropertiesPath = optionPropertiesPath;
        try {
            OPTION_PROPERTIES = loadOptionProperties();
        } catch (FileNotFoundException ex) {
            throw new RuntimeException("Unable to load option.properties file cannot continue", ex);
        }
        LOG.info("OPTION.PROPERTIES succesfully loaded");
        LOG.debug(OPTION_PROPERTIES.toString());
        smsHandler = new ClientSmsHandler();
    }

    private OptionProperties loadOptionProperties() throws FileNotFoundException {
        Gson gson = new GsonBuilder()
                .disableHtmlEscaping()
                .setFieldNamingPolicy(FieldNamingPolicy.UPPER_CAMEL_CASE)
                .setPrettyPrinting()
                .serializeNulls()
                .create();
        BufferedReader reader = new BufferedReader(new FileReader(optionPropertiesPath));
        OptionProperties data = gson.fromJson(reader, OptionProperties.class);
        return data;
    }

    public ClientSmsHandlerInterface getSmsHandler() {
        return smsHandler;
    }

}
