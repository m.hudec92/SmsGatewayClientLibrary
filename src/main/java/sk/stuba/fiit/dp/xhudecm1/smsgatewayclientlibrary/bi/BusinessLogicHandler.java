/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sk.stuba.fiit.dp.xhudecm1.smsgatewayclientlibrary.bi;

import com.google.gson.FieldNamingPolicy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.security.KeyPair;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Random;
import java.util.UUID;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.commons.lang3.ArrayUtils;
import sk.stuba.fiit.dp.xhudecm1.smsgatewayclientlibrary.SmsGatewayClient;
import static sk.stuba.fiit.dp.xhudecm1.smsgatewayclientlibrary.bi.BusinessLogicHandler.LOG;
import sk.stuba.fiit.dp.xhudecm1.smsgatewayclientlibrary.compression.CompressionHandlerClient;
import sk.stuba.fiit.dp.xhudecm1.smsgatewayclientlibrary.cyphering.CypheringCurve;
import sk.stuba.fiit.dp.xhudecm1.smsgatewayclientlibrary.exceptions.KeyExchangeAckHasInvalidMD5HashException;
import sk.stuba.fiit.dp.xhudecm1.smsgatewayclientlibrary.exceptions.KeyExchangeSeqenceNumbersDoNotMatchException;
import sk.stuba.fiit.dp.xhudecm1.smsgatewayclientlibrary.exceptions.UnableToDecryptDataException;
import sk.stuba.fiit.dp.xhudecm1.smsgatewayclientlibrary.exceptions.UnableToEncryptDataException;
import sk.stuba.fiit.dp.xhudecm1.smsgatewayclientlibrary.exceptions.UnableToGenerateAckKeyExchangePduException;

import sk.stuba.fiit.dp.xhudecm1.smsgatewayclientlibrary.exceptions.UnableToGenerateKeyPairs;
import sk.stuba.fiit.dp.xhudecm1.smsgatewayclientlibrary.exceptions.UnableToGenerateMD5HashException;
import sk.stuba.fiit.dp.xhudecm1.smsgatewayclientlibrary.exceptions.UnableToGenerateSharedSecret;
import sk.stuba.fiit.dp.xhudecm1.smsgatewayclientlibrary.exceptions.UnableToProcessReceivedTechnicalMessageException;
import sk.stuba.fiit.dp.xhudecm1.smsgatewayclientlibrary.exceptions.UnableToWriteConfigurationFileException;
import sk.stuba.fiit.dp.xhudecm1.smsgatewayclientlibrary.jsonobjects.ClientConfiguration;
import sk.stuba.fiit.dp.xhudecm1.smsgatewayclientlibrary.message.ReceivedDeliveryStatusMessage;
import sk.stuba.fiit.dp.xhudecm1.smsgatewayclientlibrary.message.ReceivedMessage;
import sk.stuba.fiit.dp.xhudecm1.smsgatewayclientlibrary.smshandler.ClientSmsHandler;
import sk.stuba.fiit.dp.xhudecm1.smsgatewayclientlibrary.smshandler.MessageReceivedCallback;
import sk.stuba.fiit.dp.xhudecm1.smsgatewayclientlibrary.utils.Utils;
import sk.stuba.fiit.dp.xhudecm1.smsgatewaysmshandler.smshandler.DeliverSmsPDU;
import sk.stuba.fiit.dp.xhudecm1.smsgatewaysmshandler.smshandler.MessageSentResultCallback;
import sk.stuba.fiit.dp.xhudecm1.smsgatewaysmshandler.smshandler.ModemInstance;
import sk.stuba.fiit.dp.xhudecm1.smsgatewaysmshandler.smshandler.SmsSecurePDU;
import sk.stuba.fiit.dp.xhudecm1.smsgatewaysmshandler.smshandler.SmsSecurePDUKey;
import sk.stuba.fiit.dp.xhudecm1.smsgatewaysmshandler.smshandler.SubmitSmsPDU;

import sk.stuba.fiit.dp.xhudecm1.smsgatewaysmshandler.smshandler.enums.KeyNegotiationStatusType;
import sk.stuba.fiit.dp.xhudecm1.smsgatewaysmshandler.smshandler.exceptions.UknownPduConentType;
import sk.stuba.fiit.dp.xhudecm1.smsgatewayclientlibrary.smshandler.DeliveryStatusMessageReceivedCallback;
import sk.stuba.fiit.dp.xhudecm1.smsgatewaydatacompression.CompressedData;
import sk.stuba.fiit.dp.xhudecm1.smsgatewaysmshandler.smshandler.DeliveryStatusSmsPDU;
import sk.stuba.fiit.dp.xhudecm1.smsgatewaysmshandler.smshandler.SmsSecurePDUData;
import sk.stuba.fiit.dp.xhudecm1.smsgatewaysmshandler.smshandler.SmsSecurePDUTechnical;

/**
 *
 * @author martinhudec
 */
public class BusinessLogicHandler {

    final static org.apache.log4j.Logger LOG = org.apache.log4j.Logger.getLogger(BusinessLogicHandler.class);

    protected final List<MessageReceivedCallback> messageReceivedCallback;
    protected final List<DeliveryStatusMessageReceivedCallback> deliveryStatusMessageReceivedCallback;
    protected ClientConfiguration clientConfiguration;
    protected final CypheringCurve cypheringCurve;
    protected final CompressionHandlerClient compressionHandler;
    protected static final String CONFIG_FILE_PATH = System.getProperty("user.home") + "/.configuration/config.json";

    protected final ClientSmsHandler smsHandler;

    public BusinessLogicHandler(ClientSmsHandler smsHandler) throws UnableToWriteConfigurationFileException {
        this.smsHandler = smsHandler;
        this.messageReceivedCallback = new CopyOnWriteArrayList<>();
        this.deliveryStatusMessageReceivedCallback = new CopyOnWriteArrayList<>();
        this.cypheringCurve = CypheringCurve.getInstance();
        this.compressionHandler = CompressionHandlerClient.getInstance();
        LOG.info("CREATING NEW BusinessLogicHandler");
        LOG.info("config file location " + CONFIG_FILE_PATH);
        initClient();
    }

    public void processReceivedDeliveryStatusMessage(DeliveryStatusSmsPDU receivedStatusPDU) {
        deliveryStatusMessageReceivedCallback.forEach((callback) -> {
            callback.messageDeliveryStatusReceived(new ReceivedDeliveryStatusMessage(
                    receivedStatusPDU.getMessageUUID(),
                    receivedStatusPDU.getReferenceNumber(),
                    receivedStatusPDU.getOriginatorNumber(),
                    receivedStatusPDU.getDeliveryStatusType()));
        });

    }

    public void processReceivedSMSMessage(DeliverSmsPDU receivedSMSPdu) throws UknownPduConentType {
        String originatorNumber = receivedSMSPdu.getOriginatorNumber();
        receivedSMSPdu.checkValidSecurePdu();
        LOG.debug("PROCESSING RECEIVED SMS MESSAGE");
        LOG.debug(receivedSMSPdu.getSecurePdu().getClass());
        if (receivedSMSPdu.getSecurePdu() instanceof SmsSecurePDUData) {
            LOG.debug("RECEIVED SMS PDU INSTANCE SECURE PDU DATA");
        } else if (receivedSMSPdu.getSecurePdu() instanceof SmsSecurePDUKey) {
            LOG.debug("RECEIVED SMS PDU INSTANCE SECURE PDU KEY");
        } else if (receivedSMSPdu.getSecurePdu() instanceof SmsSecurePDUTechnical) {
            LOG.debug("RECEIVED SMS PDU INSTANCE SECURE PDU TECHNICAL");
        }
        switch (receivedSMSPdu.getSecurePdu().getContentType()) {
            case KEY:
                processReceiverKeyMessage(receivedSMSPdu);
                break;
            case DATA:
                processReceivedDataMessage(receivedSMSPdu);
                break;
            case TECHNICAL:
                processReceivedTechnicalMessage(receivedSMSPdu);
                break;
        }
    }

    public void addMessageReceivedCallback(MessageReceivedCallback callback) {
        messageReceivedCallback.add(callback);
    }

    public void removeMessageReceivedCallback(MessageReceivedCallback callback) {
        messageReceivedCallback.remove(callback);
    }

    public void addDeliveryStatusMessageReceivedCallback(DeliveryStatusMessageReceivedCallback callback) {
        deliveryStatusMessageReceivedCallback.add(callback);
    }

    public void removeDeliveryStatusMessageReceivedCallback(DeliveryStatusMessageReceivedCallback callback) {
        deliveryStatusMessageReceivedCallback.remove(callback);
    }

    private void initClient() throws UnableToWriteConfigurationFileException {
        LOG.info("Initializing Client");
        try {
            clientConfiguration = getConfigurationFromJson();
            LOG.debug("DATA RETRIEVED FROM CONFIG FILE " + clientConfiguration);
        } catch (FileNotFoundException ex) {
            LOG.info("[PHASE 1] client library initialization");
            LOG.error("No configuration file found", ex);
            LOG.info("CREATING NEW CONFIGURATION FILE");
            try {
                KeyPair pair = cypheringCurve.generateKeyPairs();
                clientConfiguration = new ClientConfiguration(
                        Utils.toHexString(cypheringCurve.compressPrivateKey(pair.getPrivate())),
                        Utils.toHexString(cypheringCurve.compressPublicKey(pair.getPublic())),
                        SmsGatewayClient.OPTION_PROPERTIES.getApiKey(),
                        new Date(),
                        new Random().nextInt(254) + 1
                );
                writeConfigurationToJsonFile(clientConfiguration);
            } catch (UnableToGenerateKeyPairs | IOException ex1) {
                LOG.error("Error creating configuration file ", ex1);
                throw new UnableToWriteConfigurationFileException("Error creating configuration file ", ex1);

            }
        }
        if (clientConfiguration.getKeyNegotiationStatusType() != KeyNegotiationStatusType.KEYS_GENERATED) {
            startKeyExchangeProcedure();
        }
        // zapneme thread ktory bude riadit casovanu BI logiku 
        new Thread(new BiThread()).start();

    }

    public void startKeyExchangeProcedure() {
        // teraz potrebujeme poslat prvu SMSku na server s requestom na vymenu klucou a verejnym klucom ktory sa vygeneroval 
        LOG.info("[PHASE 2] client library starting key exchange procedure");
        clientConfiguration.setApiKey(SmsGatewayClient.OPTION_PROPERTIES.getApiKey());
        try {
            writeConfigurationToJsonFile(clientConfiguration);
        } catch (IOException ex) {
            LOG.error("Error writing configuration file to data storage ", ex);
        }
        SmsSecurePDU securePduKeyExchangeRequest = new SmsSecurePDUKey(
                getNextKeyExchangePduSeqNumber(),
                SmsSecurePDU.ContentTypeEnum.KEY,
                SmsSecurePDU.CypherActiveEnum.OFF,
                SmsSecurePDU.KeyExchangeStageEnum.REQUEST,
                Utils.toByteArray(clientConfiguration.getEccPublicKey()));
        SubmitSmsPDU submitSms = new SubmitSmsPDU(SmsGatewayClient.OPTION_PROPERTIES.getGatewayServerNumber(), securePduKeyExchangeRequest);
        smsHandler.sendMessage(submitSms);

        smsHandler.registerMessageSentResultCallback((UUID messageUuid, Integer referenceNumber, MessageSentResultCallback.MessageSentResultEnum result, ModemInstance modem) -> {
            if (submitSms.getMessageUUID().equals(messageUuid)) {
                if (result == MessageSentResultCallback.MessageSentResultEnum.OK) {
                    LOG.info("[PHASE 3] client CLIENT_PUB_KEY_SENT");
                    clientConfiguration.setKeyNegotiationStatusType(KeyNegotiationStatusType.CLIENT_PUB_KEY_SENT);
                } else {
                    LOG.error("[PHASE 3] client CLIENT_PUB_KEY_SENT_ERROR");
                    clientConfiguration.setKeyNegotiationStatusType(KeyNegotiationStatusType.ERROR_CLIENT_PUB_KEY_NOT_SENT);
                }
            }
            try {
                writeConfigurationToJsonFile(clientConfiguration);
            } catch (IOException ex) {
                LOG.error("Error writing configuration file to data storage ", ex);
            }
        });
    }

    public ClientConfiguration getConfigurationFromJson() throws FileNotFoundException {
        Gson gson = new GsonBuilder()
                .disableHtmlEscaping()
                .setFieldNamingPolicy(FieldNamingPolicy.UPPER_CAMEL_CASE)
                .setPrettyPrinting()
                .serializeNulls()
                .create();
        BufferedReader reader = new BufferedReader(new FileReader(CONFIG_FILE_PATH));
        ClientConfiguration data = gson.fromJson(reader, ClientConfiguration.class
        );
        return data;
    }

    public void writeConfigurationToJsonFile(ClientConfiguration clientConfiguration) throws IOException {
        Gson gson = new GsonBuilder()
                .disableHtmlEscaping()
                .setFieldNamingPolicy(FieldNamingPolicy.UPPER_CAMEL_CASE)
                .setPrettyPrinting()
                .serializeNulls()
                .create();
        File configFile = new File(CONFIG_FILE_PATH);
        configFile.getParentFile().mkdirs();
        Writer writer;
        writer = new FileWriter(configFile);
        writer.write(gson.toJson(clientConfiguration));
        writer.close();
    }

    private void processReceiverKeyMessage(DeliverSmsPDU receivedSMSPdu) throws UknownPduConentType {
        LOG.info("RECEIVED KEY MESSAGE");
        try {

            switch (((SmsSecurePDUKey) receivedSMSPdu.getSecurePdu()).getKeyExchangeStage()) {
                case RESPONSE:
                    processReceivedKeyMessageResponse(receivedSMSPdu);
                    break;
                case REQUEST:
                    LOG.error("Received unsupported type of key message " + receivedSMSPdu.toString());
                    break;
                case ACK:
                    processReceivedKeyMessageAck(receivedSMSPdu);
                    break;
                case API_KEY_ACK_SUCCESS:
                    processReceivedApiKeyMessageAckSuccess(receivedSMSPdu);
                    break;
                case API_KEY_ACK_ERROR:
                    processReceivedApiKeyError(receivedSMSPdu);
            }

        } catch (UnableToGenerateAckKeyExchangePduException | KeyExchangeAckHasInvalidMD5HashException | UnableToDecryptDataException | UnableToGenerateMD5HashException | IOException | KeyExchangeSeqenceNumbersDoNotMatchException | UnableToGenerateSharedSecret e) {
            LOG.error("Error while processing recevied KEY message", e);
        }
    }

    private void processReceivedKeyMessageResponse(DeliverSmsPDU receivedSMSPdu) throws UnableToGenerateAckKeyExchangePduException, IOException, UnableToGenerateSharedSecret, KeyExchangeSeqenceNumbersDoNotMatchException, UknownPduConentType {
        LOG.info("[PHASE 4] received Key message from server");
        if (!clientConfiguration.getKeyExchangeSeqNumber().equals(receivedSMSPdu.getSecurePdu().getSequenceNumber())) {
            // TODO send invalid seq number response message
            LOG.error("[PHASE 4] received Key message has not valid seq ID ");
            clientConfiguration.setKeyNegotiationStatusType(KeyNegotiationStatusType.ERROR_SERVER_SEQ_ID_NOT_VALID);
            writeConfigurationToJsonFile(clientConfiguration);
            throw new KeyExchangeSeqenceNumbersDoNotMatchException("Received Key exchange message response has invalid seqence number requestNumber: " + clientConfiguration.getKeyExchangeSeqNumber() + " responseNumber " + receivedSMSPdu.getSecurePdu().getSequenceNumber());
        }

        byte[] serverPublicKey = receivedSMSPdu.getSecurePdu().getPayload();
        LOG.debug("parsing received secure pdu");
        byte[] sharedSecret = cypheringCurve.generateSharedSecret(clientConfiguration.getEccPrivateKey(), serverPublicKey);
        LOG.info("SharedSecret generated OK");
        clientConfiguration.setServerEccPublicKey(Utils.toHexString(serverPublicKey));
        clientConfiguration.setKeyNegotiationStatusType(KeyNegotiationStatusType.SERVER_PUB_KEY_STORED);
        clientConfiguration.setSharedSecret(Utils.toHexString(sharedSecret));
        clientConfiguration.setAesKey(Utils.toHexString(cypheringCurve.generateAesKey(sharedSecret, Utils.toByteArray(clientConfiguration.getApiKey()))));
        LOG.info("Common AES key generated OK");
        clientConfiguration.setAesKeyModifiedAt(new Date());
        LOG.info("Storing updated clientConfiguration to json");
        writeConfigurationToJsonFile(clientConfiguration);
        LOG.info("[PHASE 5] received SERVER_PUB_KEY, new values set to clientConfiguration");
        LOG.info("Updated clinet configuration with data received in response " + clientConfiguration);
        // TERAZ JE POTREBNE ODOSLAT sifrovany ACK spet na server 

        SmsSecurePDU securePduKeyAck = null;
        try {
            byte[] encryptedData = cypheringCurve.encryptData(
                    cypheringCurve.getMD5Hash(clientConfiguration.getAesKeyByteArray()),
                    clientConfiguration.getAesKeyByteArray(),
                    clientConfiguration.getApiKeyByteArray());

            securePduKeyAck = new SmsSecurePDUKey(
                    clientConfiguration.getKeyExchangeSeqNumber(),
                    SmsSecurePDU.ContentTypeEnum.KEY,
                    SmsSecurePDU.CypherActiveEnum.OFF,
                    SmsSecurePDU.KeyExchangeStageEnum.ACK,
                    Utils.toByteArray(Utils.toHexString(encryptedData)));

        } catch (UnableToEncryptDataException | UnableToGenerateMD5HashException ex) {
            clientConfiguration.setKeyNegotiationStatusType(KeyNegotiationStatusType.ERROR_CLIENT_ACK_NOT_SENT);
            LOG.error("[PHASE 5] erro while generating new ACK response message");
            throw new UnableToGenerateAckKeyExchangePduException("Error while generating new ACK response message", ex);
        }

        SubmitSmsPDU submitSms = new SubmitSmsPDU(SmsGatewayClient.OPTION_PROPERTIES.getGatewayServerNumber(), securePduKeyAck);
        smsHandler.sendMessage(submitSms);
        LOG.info("[PHASE 5] client CLIENT_ACK_KEY submitted");
        smsHandler.registerMessageSentResultCallback((UUID messageUuid, Integer referenceNumber, MessageSentResultCallback.MessageSentResultEnum result, ModemInstance modem) -> {
            if (submitSms.getMessageUUID().equals(messageUuid)) {
                if (result == MessageSentResultCallback.MessageSentResultEnum.OK) {
                    clientConfiguration.setKeyNegotiationStatusType(KeyNegotiationStatusType.CLIENT_ACK_SENT);
                    LOG.info("[PHASE 6] CLIENT_ACK_KEY secure PDU succesfully sent");
                } else {
                    clientConfiguration.setKeyNegotiationStatusType(KeyNegotiationStatusType.ERROR_CLIENT_ACK_NOT_SENT);
                    LOG.error("[PHASE 6] Error while sending ACK secure PDU");
                }
            }
            try {
                writeConfigurationToJsonFile(clientConfiguration);
            } catch (IOException ex) {
                LOG.error("Error writing configuration file to data storage ", ex);
            }
        });
    }

    private void processReceivedKeyMessageAck(DeliverSmsPDU receivedSMSPdu) throws IOException, KeyExchangeSeqenceNumbersDoNotMatchException, UnableToGenerateMD5HashException, UnableToDecryptDataException, UknownPduConentType {
        LOG.info("[PHASE 7]RECEIVED KEY MESSAGE ACK");
        try {
            clientConfiguration.setKeyNegotiationStatusType(KeyNegotiationStatusType.SERVER_ACK_RECEIVED);

            writeConfigurationToJsonFile(clientConfiguration);

            byte[] md5HashDecrypted = cypheringCurve.decryptData(receivedSMSPdu.getSecurePdu().getPayload(),
                    clientConfiguration.getAesKeyByteArray(),
                    clientConfiguration.getApiKeyByteArray());

            byte[] md5HashLocal = cypheringCurve.getMD5Hash(clientConfiguration.getAesKeyByteArray());

            if (Arrays.equals(md5HashLocal, md5HashDecrypted)) {
                LOG.info("[PHASE 7] MD5 hash OK, setting new client configuration with status KEYS_GENERATED");
                clientConfiguration.setKeyNegotiationStatusType(KeyNegotiationStatusType.KEYS_GENERATED);
                LOG.info("[PHASE 8] Sending temporarily stored messages");
                smsHandler.sendTemporarilyStoredMessages();
            } else {
                LOG.error("[PHASE 7] invalid MD5 Hash received");
                clientConfiguration.setKeyNegotiationStatusType(KeyNegotiationStatusType.ERROR_SERVER_ACK_HASH_NOT_VALID);
                throw new KeyExchangeSeqenceNumbersDoNotMatchException("Received ACK has invalid MD5 hash");
            }
            writeConfigurationToJsonFile(clientConfiguration);
        } catch (UnableToGenerateMD5HashException | UnableToDecryptDataException ex) {
            LOG.error("[PHASE 7] error while processing received KEY Message ACK ", ex);
            clientConfiguration.setKeyNegotiationStatusType(KeyNegotiationStatusType.ERROR);
            writeConfigurationToJsonFile(clientConfiguration);
            throw ex;
        }

    }

    private void processReceivedDataMessage(DeliverSmsPDU receivedSMSPdu) throws UknownPduConentType {
        LOG.debug("Processing received secure message DATA " + receivedSMSPdu.toString());
        try {
            SmsSecurePDUData securePduData = (SmsSecurePDUData) receivedSMSPdu.getSecurePdu();
            LOG.debug("secure pdu data " + securePduData.toString());
            // v pripade ze nebolo nastavene sifrovanie tak netreba desifrovat obsah
            byte[] decrypted = receivedSMSPdu.getSecurePdu().getPayload();
            if (receivedSMSPdu.getSecurePdu().getCypherActive().equals(SmsSecurePDU.CypherActiveEnum.ON)) {
                LOG.debug("ENCRYPTED " + new String(receivedSMSPdu.getSecurePdu().getPayload()));
                decrypted = decryptData(securePduData.getPayload());
                LOG.debug("DECRYPTED " + new String(decrypted));
            }
            securePduData.setDecryptedPayload(decrypted);
            byte[] decompressed = compressionHandler.decompressData(decrypted, securePduData.getCompressionType(), null);
            LOG.debug("DECOMPRESSED " + new String(decompressed));
            securePduData.setDecompressedPayload(decompressed);

            for (MessageReceivedCallback callback : messageReceivedCallback) {
                callback.messageReceived(new ReceivedMessage(((SmsSecurePDUData) securePduData).getPureData(),
                        securePduData,
                        receivedSMSPdu.getMessageUUID()));
            }
        } catch (UnableToDecryptDataException ex) {
            Logger.getLogger(BusinessLogicHandler.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(BusinessLogicHandler.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void processReceivedTechnicalMessage(DeliverSmsPDU receivedSMSPdu) throws UknownPduConentType {

        SmsSecurePDUTechnical technicalReceivedMessage = (SmsSecurePDUTechnical) receivedSMSPdu.getSecurePdu();
        switch (technicalReceivedMessage.getTechnicalMessageType()) {
            case PING:
                processReceivedTechnicalPingMessage(receivedSMSPdu);
                break;
            case REQUEST_KEY_EXCHANGE:
                processReceivedTechnicalKeyExchangeRequestMessage(receivedSMSPdu);
        }
    }

    private void processReceivedTechnicalPingMessage(DeliverSmsPDU receivedSMSPdu) {
        LOG.info("[PHASE 1] received TECHNICAL PING message from server");
        try {

            SmsSecurePDUTechnical technicalReceivedMessage = (SmsSecurePDUTechnical) receivedSMSPdu.getSecurePdu();
            LOG.debug("secure pdu data " + technicalReceivedMessage.toString());
            LOG.debug("ENCRYPTED " + new String(receivedSMSPdu.getSecurePdu().getPayload()));
            byte[] decrypted = decryptData(technicalReceivedMessage.getPayload());
            LOG.debug("DECRYPTED " + new String(decrypted));
            technicalReceivedMessage.setDecryptedPayload(decrypted);
            byte[] decompressed = compressionHandler.decompressData(decrypted, technicalReceivedMessage.getCompressionType(), null);
            LOG.debug("DECOMPRESSED " + new String(decompressed));
            technicalReceivedMessage.setDecompressedPayload(decompressed);
            CompressedData compressed = compressionHandler.compressData(technicalReceivedMessage.getPureData(), null);
            byte[] encryptedData = encryptData(compressed.getCompressed());

            SmsSecurePDUTechnical securePduTechnicalPingAck = new SmsSecurePDUTechnical(
                    clientConfiguration.getPduSequenceNumber(),
                    SmsSecurePDU.ContentTypeEnum.TECHNICAL,
                    SmsSecurePDU.CypherActiveEnum.ON,
                    compressed.getCompressionType(),
                    SmsSecurePDU.TechnicalMessageTypeEnum.PING,
                    Utils.toByteArray(Utils.toHexString(encryptedData)));

            SubmitSmsPDU submitSms = new SubmitSmsPDU(SmsGatewayClient.OPTION_PROPERTIES.getGatewayServerNumber(), securePduTechnicalPingAck);
            smsHandler.sendMessage(submitSms);
            LOG.info("[PHASE 2] client CLIENT_TECHNICAL_PING  submitted");
            smsHandler.registerMessageSentResultCallback((UUID messageUuid, Integer referenceNumber, MessageSentResultCallback.MessageSentResultEnum result, ModemInstance modem) -> {
                if (submitSms.getMessageUUID().equals(messageUuid)) {
                    if (result == MessageSentResultCallback.MessageSentResultEnum.OK) {
                        LOG.info("[PHASE 3] PING secure PDU succesfully sent");
                    } else {
                        LOG.error("[PHASE 3] Error while sending PING secure PDU");
                    }
                }
            });
        } catch (UnableToDecryptDataException | IOException | UknownPduConentType | UnableToEncryptDataException ex) {
            LOG.error("Unable to process received technical message", ex);
        }

    }

    private void processReceivedTechnicalKeyExchangeRequestMessage(DeliverSmsPDU receivedSMSPdu) {
        LOG.info("[PHASE 1] received TECHNICAL KEY EXCHANGE REQUEST message from server");
        try {

            SmsSecurePDUTechnical technicalReceivedMessage = (SmsSecurePDUTechnical) receivedSMSPdu.getSecurePdu();
            LOG.debug("secure pdu data " + technicalReceivedMessage.toString());
            LOG.debug("ENCRYPTED " + new String(receivedSMSPdu.getSecurePdu().getPayload()));
            byte[] decrypted = decryptData(technicalReceivedMessage.getPayload());
            LOG.debug("DECRYPTED " + Utils.toHexString(decrypted));
            technicalReceivedMessage.setDecryptedPayload(decrypted);
            byte[] decompressed = compressionHandler.decompressData(decrypted, technicalReceivedMessage.getCompressionType(), null);
            LOG.debug("DECOMPRESSED " + Utils.toHexString(decompressed));
            technicalReceivedMessage.setDecompressedPayload(decompressed);

            byte[] md5HashLocal = cypheringCurve.getMD5Hash(clientConfiguration.getAesKeyByteArray());

            if (!Arrays.equals(md5HashLocal, technicalReceivedMessage.getPureData())) {
                LOG.error("Received technical message with key exchange request has invalid md5 hash");
                return;
            }
            startKeyExchangeProcedure();
        } catch (UnableToDecryptDataException | IOException | UknownPduConentType ex) {
            LOG.error("Unable to process received technical message", ex);
        } catch (UnableToGenerateMD5HashException ex) {
            Logger.getLogger(BusinessLogicHandler.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public byte[] encryptData(byte[] data) throws UnableToEncryptDataException {
        LOG.debug("DATA TO ENCRYPT " + Utils.toHexString(data));
        byte[] encrypted = cypheringCurve.encryptData(data, clientConfiguration.getAesKeyByteArray(), clientConfiguration.getApiKeyByteArray());
        LOG.debug("ENCRYPTED DATA " + Utils.toHexString(encrypted));
        return encrypted;
    }

    public byte[] decryptData(byte[] data) throws UnableToDecryptDataException {
        LOG.debug("DATA TO DECRYPT " + Utils.toHexString(data));
        byte[] decrypted = cypheringCurve.decryptData(data, clientConfiguration.getAesKeyByteArray(), clientConfiguration.getApiKeyByteArray());
        LOG.debug("DECRYPTED DATA " + Utils.toHexString(decrypted));
        return decrypted;
    }

    synchronized public Integer getNextPduSeqNumber() {
        Integer seqNum = clientConfiguration.getPduSequenceNumber();
        clientConfiguration.setPduSequenceNumber(++seqNum % 255);
        try {
            writeConfigurationToJsonFile(clientConfiguration);
        } catch (IOException ex) {
            LOG.error("Error while writing new seqNumber to file", ex);
        }
        LOG.debug("current seq num " + seqNum);
        return seqNum;
    }

    synchronized public Integer getNextKeyExchangePduSeqNumber() {
        Integer seqNum = clientConfiguration.getKeyExchangeSeqNumber();
        clientConfiguration.setKeyExchangeSeqNumber(++seqNum % 255);
        try {
            writeConfigurationToJsonFile(clientConfiguration);
        } catch (IOException ex) {
            LOG.error("Error while writing new seqNumber to file", ex);
        }
        LOG.debug("current seq num " + seqNum);
        return seqNum;
    }

    private void processReceivedApiKeyMessageAckSuccess(DeliverSmsPDU receivedSMSPdu) throws UknownPduConentType, IOException, KeyExchangeSeqenceNumbersDoNotMatchException, UnableToGenerateMD5HashException, UnableToDecryptDataException, UnableToGenerateAckKeyExchangePduException {
        LOG.info("[PHASE 6] API KEY ACK SUCCESSreceived ");
        try {
            if (!clientConfiguration.getKeyExchangeSeqNumber().equals(receivedSMSPdu.getSecurePdu().getSequenceNumber())) {
                // TODO send invalid seq number response message
                LOG.error("[PHASE 7] API KEY ACK has invalid sequence number ");
                clientConfiguration.setKeyRegenerationStatusType(KeyNegotiationStatusType.ERROR_SERVER_SEQ_ID_NOT_VALID);
                writeConfigurationToJsonFile(clientConfiguration);
                throw new KeyExchangeSeqenceNumbersDoNotMatchException("Received Api Key exchange message response has invalid seqence number requestNumber: " + clientConfiguration.getKeyExchangeSeqNumber() + " responseNumber " + receivedSMSPdu.getSecurePdu().getSequenceNumber());
            }

            clientConfiguration.setKeyRegenerationStatusType(KeyNegotiationStatusType.NEW_API_KEY_ACK_RECEIVED);

            writeConfigurationToJsonFile(clientConfiguration);

            byte[] md5HashDecrypted = decryptData(receivedSMSPdu.getSecurePdu().getPayload());

            byte[] md5HashLocal = cypheringCurve.getMD5Hash(clientConfiguration.getNewAesKeyByteArray());

            if (!Arrays.equals(md5HashLocal, md5HashDecrypted)) {
                LOG.error("[PHASE 8] API KEY ACK has invalid md5 hash ");
                clientConfiguration.setKeyRegenerationStatusType(KeyNegotiationStatusType.ERROR_SERVER_ACK_HASH_NOT_VALID);
                writeConfigurationToJsonFile(clientConfiguration);
                throw new KeyExchangeSeqenceNumbersDoNotMatchException("Received API key ACK has invalid MD5 hash");
            }
            LOG.info("[PHASE 8] API KEY ACK has valid md5 hash ");

            // TERAZ UZ VIEME ZE JE VSETKO OK a server si nastavil nove kluce 
            applyNewKeys();
            clientConfiguration.setKeyRegenerationStatusType(KeyNegotiationStatusType.NEW_API_KEY_GENERATED);
            writeConfigurationToJsonFile(clientConfiguration);
        } catch (UnableToGenerateMD5HashException | UnableToDecryptDataException ex) {
            clientConfiguration.setKeyRegenerationStatusType(KeyNegotiationStatusType.ERROR);
            writeConfigurationToJsonFile(clientConfiguration);
            throw ex;
        }
    }

    private void processReceivedApiKeyError(DeliverSmsPDU receivedSMSPdu) throws UknownPduConentType, IOException, KeyExchangeSeqenceNumbersDoNotMatchException, UnableToGenerateMD5HashException, UnableToGenerateMD5HashException, UnableToDecryptDataException, KeyExchangeAckHasInvalidMD5HashException, UnableToDecryptDataException, UnableToDecryptDataException, UnableToDecryptDataException, KeyExchangeAckHasInvalidMD5HashException, KeyExchangeAckHasInvalidMD5HashException {
        LOG.info("[PHASE 6] API KEY ACK ERROR received ");
        try {
            if (!clientConfiguration.getKeyExchangeSeqNumber().equals(receivedSMSPdu.getSecurePdu().getSequenceNumber())) {
                // TODO send invalid seq number response message
                LOG.error("[PHASE 7] API KEY ACK has invalid sequence number ");
                clientConfiguration.setKeyRegenerationStatusType(KeyNegotiationStatusType.ERROR);
                writeConfigurationToJsonFile(clientConfiguration);
                throw new KeyExchangeSeqenceNumbersDoNotMatchException("Received Api Key exchange message response has invalid seqence number requestNumber: " + clientConfiguration.getKeyExchangeSeqNumber() + " responseNumber " + receivedSMSPdu.getSecurePdu().getSequenceNumber());
            }

            byte[] md5HashDecrypted = decryptData(receivedSMSPdu.getSecurePdu().getPayload());

            byte[] md5HashLocal = cypheringCurve.getMD5Hash(clientConfiguration.getAesKeyByteArray());

            if (!Arrays.equals(md5HashLocal, md5HashDecrypted)) {
                LOG.error("[PHASE 8] API KEY ACK ERROR has invalid md5 hash ");

                throw new KeyExchangeAckHasInvalidMD5HashException("Received API key ACK has invalid MD5 hash");
            }

            clientConfiguration.setKeyRegenerationStatusType(KeyNegotiationStatusType.ERROR);

            writeConfigurationToJsonFile(clientConfiguration);

            LOG.info("[PHASE 8] API KEY ACK ERROR set to key exchange");

        } catch (UnableToGenerateMD5HashException | UnableToDecryptDataException ex) {
            throw ex;
        }

    }

    private class BiThread implements Runnable {

        public BiThread() {
        }

        @Override
        public void run() {
            while (true) {
                if (clientConfiguration.getKeyNegotiationStatusType() == KeyNegotiationStatusType.KEYS_GENERATED) {
                    checkApiKeyValidity();
                }
                try {
                    Thread.sleep(SmsGatewayClient.OPTION_PROPERTIES.getApiKeyDayValidityPeriod() * 1000 * 60);
                } catch (InterruptedException ex) {
                    Logger.getLogger(BusinessLogicHandler.class.getName()).log(Level.SEVERE, null, ex);
                }

            }
        }

    }

    private void checkApiKeyValidity() {
        Date lastApiKeyModification = clientConfiguration.getApiKeyModifiedAt();
        Date now = new Date();
        LOG.info("[PHASE 1] checking api key validity current api key is old " + TimeUnit.DAYS.convert(now.getTime() - lastApiKeyModification.getTime(), TimeUnit.MILLISECONDS) + "  days ");
        if (TimeUnit.DAYS.convert(now.getTime() - lastApiKeyModification.getTime(), TimeUnit.MILLISECONDS) >= SmsGatewayClient.OPTION_PROPERTIES.getApiKeyDayValidityPeriod()) {
            try {
                // teraz potrebujeme poslat prvu SMSku na server s requestom na vymenu klucou a verejnym klucom ktory sa vygeneroval
                LOG.info("[PHASE 2] Generating new Api keys");
                byte[] newApiKey = cypheringCurve.compressPublicKey(cypheringCurve.generateKeyPairs().getPublic());
                clientConfiguration.setNewApiKey(Utils.toHexString(newApiKey));
                clientConfiguration.setNewApiKeyModifiedAt(new Date());
                clientConfiguration.setNewAesKey(Utils.toHexString(cypheringCurve.generateAesKey(clientConfiguration.getSharedSecretByteArray(), newApiKey)));
                clientConfiguration.setNewAesKeyModifiedAt(new Date());
                clientConfiguration.setKeyRegenerationStatusType(KeyNegotiationStatusType.NEW_API_KEY_PREPARED);
                writeConfigurationToJsonFile(clientConfiguration);

                startApiKeyExchange();
            } catch (UnableToGenerateKeyPairs | UnableToGenerateSharedSecret | IOException | UnableToEncryptDataException | UnableToGenerateMD5HashException ex) {
                LOG.fatal("FATAL Error during starting new API key exchange", ex);
            }

        }
    }

    private void startApiKeyExchange() throws UnableToEncryptDataException, UnableToGenerateMD5HashException {
        LOG.info("[PHASE 3] Starting API KEY exchange");
        byte[] newAesKeyMd5 = cypheringCurve.getMD5Hash(clientConfiguration.getNewAesKeyByteArray());
        byte[] concatenatedPayload = ArrayUtils.addAll(clientConfiguration.getNewApiKeyByteArray(), newAesKeyMd5);
        LOG.debug("Concat payload " + Utils.toHexString(concatenatedPayload));
        SmsSecurePDU securePduApiKeyExchangeRequest = new SmsSecurePDUKey(
                getNextKeyExchangePduSeqNumber(),
                SmsSecurePDU.ContentTypeEnum.KEY,
                SmsSecurePDU.CypherActiveEnum.ON,
                SmsSecurePDU.KeyExchangeStageEnum.API_KEY_REQUEST,
                encryptData(concatenatedPayload));

        SubmitSmsPDU submitSms = new SubmitSmsPDU(SmsGatewayClient.OPTION_PROPERTIES.getGatewayServerNumber(), securePduApiKeyExchangeRequest);
        smsHandler.sendMessage(submitSms);
        LOG.info("[PHASE 4] API KEY REQUEST sent ");
        smsHandler.registerMessageSentResultCallback((UUID messageUuid, Integer referenceNumber, MessageSentResultCallback.MessageSentResultEnum result, ModemInstance modem) -> {
            if (submitSms.getMessageUUID().equals(messageUuid)) {
                if (result == MessageSentResultCallback.MessageSentResultEnum.OK) {
                    clientConfiguration.setKeyRegenerationStatusType(KeyNegotiationStatusType.NEW_API_KEY_SENT);
                    LOG.info("[PHASE 5] API KEY REQUEST successfully sent  ");
                } else {
                    clientConfiguration.setKeyRegenerationStatusType(KeyNegotiationStatusType.ERROR_NEW_API_KEY_NOT_SENT);
                    LOG.error("[PHASE 5] Error sending API KEY REQUEST  ");
                }
            }
            try {
                writeConfigurationToJsonFile(clientConfiguration);
            } catch (IOException ex) {
                LOG.error("Error writing configuration file to data storage ", ex);
            }
        });
        clientConfiguration.setKeyRegenerationStatusType(KeyNegotiationStatusType.NEW_API_KEY_ACK_SENT);
    }

    public boolean isAbleToSendSecureMessage() {
        if (clientConfiguration.getKeyRegenerationStatusType() != null && clientConfiguration.getKeyRegenerationStatusType() != KeyNegotiationStatusType.NEW_API_KEY_GENERATED) {
            switch (clientConfiguration.getKeyRegenerationStatusType()) {
                case NEW_API_KEY_SENT:
                case NEW_API_KEY_PREPARED:
                case NEW_API_KEY_ACK_RECEIVED:
                    LOG.debug("Unable to send secure message, wrong client api key exchange state " + clientConfiguration.getKeyRegenerationStatusType());
                    new Exception("Stack").printStackTrace();
                    return false;
            }

        }
        if (clientConfiguration.getKeyNegotiationStatusType() != KeyNegotiationStatusType.KEYS_GENERATED) {
            LOG.error("Unable to send secure message, wrong client key exchange state " + clientConfiguration.getKeyNegotiationStatusType());
            new Exception("Stack").printStackTrace();
            return false;
        } else {
            return true;
        }
    }

    private void applyNewKeys() {
        LOG.info("Applying new Keys");
        clientConfiguration.setAesKey(clientConfiguration.getNewAesKey());
        clientConfiguration.setApiKey(clientConfiguration.getNewApiKey());
        clientConfiguration.setAesKeyModifiedAt(new Date());
        clientConfiguration.setApiKeyModifiedAt(new Date());
        try {
            writeConfigurationToJsonFile(clientConfiguration);
        } catch (IOException ex) {
            LOG.error("Error writing new keys to JSON config file", ex);
        }
    }
}
