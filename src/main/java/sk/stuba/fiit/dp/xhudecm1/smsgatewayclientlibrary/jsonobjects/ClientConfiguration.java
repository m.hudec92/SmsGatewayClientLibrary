/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sk.stuba.fiit.dp.xhudecm1.smsgatewayclientlibrary.jsonobjects;

import com.google.gson.annotations.Expose;
import java.util.Date;
import sk.stuba.fiit.dp.xhudecm1.smsgatewayclientlibrary.utils.Utils;
import sk.stuba.fiit.dp.xhudecm1.smsgatewaysmshandler.smshandler.enums.KeyNegotiationStatusType;

/**
 *
 * @author martinhudec
 */
public class ClientConfiguration {

    @Expose
    private String eccPublicKey;
    @Expose
    private String eccPrivateKey;
    @Expose
    private String aesKey;
    @Expose
    private String newAesKey;
    @Expose
    private Date aesKeyModifiedAt;
    @Expose
    private Date newAesKeyModifiedAt;
    @Expose
    private String apiKey;
    @Expose
    private String newApiKey;
    @Expose
    private Date apiKeyModifiedAt;
    @Expose
    private Date newApiKeyModifiedAt;
    @Expose
    private String serverEccPublicKey;
    @Expose
    private String sharedSecret;
    @Expose
    private KeyNegotiationStatusType keyNegotiationStatusType;
    @Expose
    private KeyNegotiationStatusType keyRegenerationStatusType;
    @Expose
    private Integer keyExchangeSeqNumber;
    @Expose
    private Integer pduSequenceNumber;

    public ClientConfiguration() {
    }

    public ClientConfiguration(String eccPrivateKey, String eccPublicKey, String apiKey, Date apiKeyModifiedAt, Integer keyExchangeSeqNumber) {
        this.eccPublicKey = eccPublicKey;
        this.eccPrivateKey = eccPrivateKey;
        this.apiKey = apiKey;
        this.apiKeyModifiedAt = apiKeyModifiedAt;
        this.keyExchangeSeqNumber = keyExchangeSeqNumber;
        this.pduSequenceNumber = 0;
    }

    public void setEccPublicKey(String eccPublicKey) {
        this.eccPublicKey = eccPublicKey;
    }

    public void setEccPrivateKey(String eccPrivateKey) {
        this.eccPrivateKey = eccPrivateKey;
    }

    public void setApiKey(String apiKey) {
        this.apiKey = apiKey;
    }

    public String getAesKey() {
        return aesKey;
    }

    public void setAesKey(String aesKey) {
        this.aesKey = aesKey;
    }

    public Date getAesKeyModifiedAt() {
        return aesKeyModifiedAt;
    }

    public void setAesKeyModifiedAt(Date aesKeyModifiedAt) {
        this.aesKeyModifiedAt = aesKeyModifiedAt;
    }

    public Date getApiKeyModifiedAt() {
        return apiKeyModifiedAt;
    }

    public void setApiKeyModifiedAt(Date apiKeyModifiedAt) {
        this.apiKeyModifiedAt = apiKeyModifiedAt;
    }

    public String getServerEccPublicKey() {
        return serverEccPublicKey;
    }

    public void setServerEccPublicKey(String serverEccPublicKey) {
        this.serverEccPublicKey = serverEccPublicKey;
    }

    public String getSharedSecret() {
        return sharedSecret;
    }

    public byte[] getSharedSecretByteArray() {
        return Utils.toByteArray(sharedSecret);

    }

    public void setSharedSecret(String sharedSecret) {
        this.sharedSecret = sharedSecret;
    }

    public KeyNegotiationStatusType getKeyNegotiationStatusType() {
        return keyNegotiationStatusType;
    }

    public void setKeyNegotiationStatusType(KeyNegotiationStatusType keyNegotiationStatusType) {
        this.keyNegotiationStatusType = keyNegotiationStatusType;
    }

    public String getEccPublicKey() {
        return eccPublicKey;
    }

    public String getEccPrivateKey() {
        return eccPrivateKey;
    }

    public String getApiKey() {
        return apiKey;
    }

    public byte[] getAesKeyByteArray() {
        return Utils.toByteArray(aesKey);
    }

    public byte[] getEccPublicKeyByteArray() {
        return Utils.toByteArray(eccPublicKey);
    }

    public byte[] getEccPrivateKeyByteArray() {
        return Utils.toByteArray(eccPrivateKey);
    }

    public byte[] getApiKeyByteArray() {
        return Utils.toByteArray(apiKey);

    }

    public void setKeyExchangeSeqNumber(Integer keyExchangeSeqNumber) {
        this.keyExchangeSeqNumber = keyExchangeSeqNumber;
    }

    public Integer getKeyExchangeSeqNumber() {
        return keyExchangeSeqNumber;
    }

    public Integer getPduSequenceNumber() {
        return pduSequenceNumber;
    }

    public void setPduSequenceNumber(Integer pduSequenceNumber) {
        this.pduSequenceNumber = pduSequenceNumber;
    }

    public String getNewAesKey() {
        return newAesKey;
    }

    public byte[] getNewAesKeyByteArray() {
        return Utils.toByteArray(newAesKey);
    }

    public void setNewAesKey(String newAesKey) {
        this.newAesKey = newAesKey;
    }

    public Date getNewAesKeyModifiedAt() {
        return newAesKeyModifiedAt;
    }

    public void setNewAesKeyModifiedAt(Date newAesKeyModifiedAt) {
        this.newAesKeyModifiedAt = newAesKeyModifiedAt;
    }

    public String getNewApiKey() {
        return newApiKey;
    }

    public byte[] getNewApiKeyByteArray() {
        return Utils.toByteArray(newApiKey);
    }

    public void setNewApiKey(String newApiKey) {
        this.newApiKey = newApiKey;
    }

    public Date getNewApiKeyModifiedAt() {
        return newApiKeyModifiedAt;
    }

    public void setNewApiKeyModifiedAt(Date newApiKeyModifiedAt) {
        this.newApiKeyModifiedAt = newApiKeyModifiedAt;
    }

    public KeyNegotiationStatusType getKeyRegenerationStatusType() {
        return keyRegenerationStatusType;
    }

    public void setKeyRegenerationStatusType(KeyNegotiationStatusType keyRegenerationStatusType) {
        this.keyRegenerationStatusType = keyRegenerationStatusType;
    }

    @Override
    public String toString() {
        return "Configuration [eccPublicKey=" + eccPublicKey + ", eccPrivateKey=" + eccPrivateKey + ", "
                + "aesKey=" + aesKey + ", aeskeyModifiedAt=" + aesKeyModifiedAt + ", apiKey=" + apiKey
                + ", apiKeyModifiedAt=" + apiKeyModifiedAt + ", serverEccPublicKey=" + serverEccPublicKey
                + ", sharedSecret=" + sharedSecret + ", keyNegotiationStatusType=" + keyNegotiationStatusType
                + ", keyExchangeSeqNumber=" + keyExchangeSeqNumber
                + ", pduSequenceNumber=" + pduSequenceNumber
                + ", newApiKey=" + newApiKey
                + ", newApiKeyModifiedAt=" + newApiKeyModifiedAt
                + ", newAesKey=" + newAesKey
                + ", newAesKeyModifiedAt=" + newAesKeyModifiedAt
                + ", keyRegenerationStatusType=" + keyRegenerationStatusType + "]";
    }
}
