/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sk.stuba.fiit.dp.xhudecm1.smsgatewayclientlibrary.jsonobjects;

import com.google.gson.annotations.Expose;
import java.util.List;
import sk.stuba.fiit.dp.xhudecm1.smsgatewayclientlibrary.utils.Utils;

/**
 *
 * @author martinhudec
 */
public class OptionProperties {

    private final static org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(OptionProperties.class);
    @Expose
    private String gatewayServerNumber;
    @Expose
    private String apiKey;
    @Expose
    private List<Modems> modems;
    @Expose
    private String temporaryMessageStoragePath;
    @Expose
    private Integer apiKeyDayValidityPeriod;
    @Expose
    private String customAlphabetStoragePath;

    public OptionProperties() {
    }

    public OptionProperties(String gatewayServerNumber, String apiKey, List<Modems> modems, String temporaryMessageStoragePath, Integer apiKeyDayValidityPeriod, String customAlphabetPath) {
        this.gatewayServerNumber = gatewayServerNumber;
        this.apiKey = apiKey;
        this.modems = modems;
        this.temporaryMessageStoragePath = temporaryMessageStoragePath;
        this.apiKeyDayValidityPeriod = apiKeyDayValidityPeriod;
        this.customAlphabetStoragePath = customAlphabetPath;
    }

    public List<Modems> getModems() {
        return modems;
    }

    public void setModems(List<Modems> modems) {
        this.modems = modems;
    }

    public void setApiKey(String apiKey) {
        this.apiKey = apiKey;
    }

    public void setGatewayServerNumber(String GatewayServerNumber) {
        this.gatewayServerNumber = GatewayServerNumber;
    }

    public String getGatewayServerNumber() {
        return gatewayServerNumber;
    }

    public String getApiKey() {
        return apiKey;
    }

    public byte[] getApiKeyByteArray() {
        return Utils.toByteArray(apiKey);

    }

    public String getTemporaryMessageStoragePath() {
        return temporaryMessageStoragePath;
    }

    public void setTemporaryMessageStoragePath(String temporaryMessageStoragePath) {
        this.temporaryMessageStoragePath = temporaryMessageStoragePath;
    }

    public Integer getApiKeyDayValidityPeriod() {
        return apiKeyDayValidityPeriod;
    }

    public void setApiKeyDayValidityPeriod(Integer apiKeyDayValidityPeriod) {
        this.apiKeyDayValidityPeriod = apiKeyDayValidityPeriod;
    }

    public String getCustomAlphabetPath() {
        return customAlphabetStoragePath;
    }

    public void setCustomAlphabetPath(String customAlphabetPath) {
        this.customAlphabetStoragePath = customAlphabetPath;
    }

    @Override
    public String toString() {
        return "OptionProperties [apiKey=" + apiKey
                + ", gatewayServerNumber=" + gatewayServerNumber
                + ", modems=" + modems
                + ", temporaryMessageStoragePath=" + temporaryMessageStoragePath
                + ", apiKeyDayValidityPeriod=" + apiKeyDayValidityPeriod
                + ", customAlphabetPath=" + customAlphabetStoragePath + "]";
    }

    public class Modems {

        @Expose
        private String name;
        @Expose
        private String manufacturer;
        @Expose
        private String simPin;
        @Expose
        private String serialPort;
        @Expose
        private String operator;

        public Modems() {
        }

        public Modems(String name, String manufacturer, String simPin, String serialPort, String operator) {
            this.name = name;
            this.manufacturer = manufacturer;
            this.simPin = simPin;
            this.serialPort = serialPort;
            this.operator = operator;
        }

        public String getSimPin() {
            return simPin;
        }

        public void setSimPin(String simPin) {
            this.simPin = simPin;
        }

        public String getName() {

            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getManufacturer() {
            return manufacturer;
        }

        public void setManufacturer(String manufacturer) {
            this.manufacturer = manufacturer;
        }

        public String getSerialPort() {
            return serialPort;
        }

        public void setSerialPort(String serialPort) {
            this.serialPort = serialPort;
        }

        public String getOperator() {
            return operator;
        }

        public void setOperator(String Operator) {
            this.operator = Operator;
        }

        @Override
        public String toString() {
            return "(name=" + name
                    + ", manufacturer=" + manufacturer
                    + ", serialPort=" + serialPort
                    + ", operator=" + operator
                    + ", simPin=" + simPin + ")";
        }
    }
}
