/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sk.stuba.fiit.dp.xhudecm1.smsgatewayclientlibrary.loadbalancer;

import java.util.List;
import sk.stuba.fiit.dp.xhudecm1.smsgatewayclientlibrary.bi.BusinessLogicHandler;
import sk.stuba.fiit.dp.xhudecm1.smsgatewaysmshandler.smshandler.ModemInstance;
import sk.stuba.fiit.dp.xhudecm1.smsgatewaysmshandler.smshandler.SubmitSmsPDU;

/**
 *
 * @author martinhudec
 */
public class LoadBalancer implements LoadBalancerInterface {

    private final List<ModemInstance> modemInstanceList;
    final static org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(LoadBalancer.class);

    public LoadBalancer(List<ModemInstance> modemInstanceList) {
        logger.info("CREATING NEW LoadBalancer");
        this.modemInstanceList = modemInstanceList;

    }

    @Override
    public ModemInstance getAppropriateModem(SubmitSmsPDU submitPdu) {
        return modemInstanceList.get(0);
    }

}
