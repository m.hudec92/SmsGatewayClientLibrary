/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sk.stuba.fiit.dp.xhudecm1.smsgatewayclientlibrary.cyphering;

import java.io.OutputStream;
import java.math.BigInteger;
import java.nio.ByteBuffer;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.KeyFactory;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.SecureRandom;
import java.security.Security;
import java.security.spec.ECGenParameterSpec;
import java.security.spec.InvalidKeySpecException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.KeyAgreement;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.spec.SecretKeySpec;
import org.bouncycastle.crypto.CipherParameters;
import org.bouncycastle.crypto.params.KeyParameter;
import org.bouncycastle.crypto.params.ParametersWithIV;
import org.bouncycastle.jce.ECNamedCurveTable;
import org.bouncycastle.jce.interfaces.ECPrivateKey;
import org.bouncycastle.jce.interfaces.ECPublicKey;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.bouncycastle.jce.spec.ECParameterSpec;
import org.bouncycastle.jce.spec.ECPrivateKeySpec;
import org.bouncycastle.jce.spec.ECPublicKeySpec;
import org.bouncycastle.math.ec.ECCurve;
import org.bouncycastle.util.encoders.Hex;
import sk.stuba.fiit.dp.xhudecm1.smsgatewayclientlibrary.exceptions.UnableToEncryptDataException;
import sk.stuba.fiit.dp.xhudecm1.smsgatewayclientlibrary.exceptions.UnableToDecompressKey;
import sk.stuba.fiit.dp.xhudecm1.smsgatewayclientlibrary.exceptions.UnableToDecryptDataException;

import sk.stuba.fiit.dp.xhudecm1.smsgatewayclientlibrary.exceptions.UnableToGenerateKeyPairs;
import sk.stuba.fiit.dp.xhudecm1.smsgatewayclientlibrary.exceptions.UnableToGenerateMD5HashException;
import sk.stuba.fiit.dp.xhudecm1.smsgatewayclientlibrary.exceptions.UnableToGenerateSharedSecret;
import sk.stuba.fiit.dp.xhudecm1.smsgatewayclientlibrary.utils.Utils;

/**
 *
 * @author martinhudec
 */
public class CypheringCurve {

    final static org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(CypheringCurve.class);
    private static CypheringCurve instance;

    public CypheringCurve() {
        logger.info("Adding security provider " + BouncyCastleProvider.class.getName());
        Security.addProvider(new BouncyCastleProvider());

    }

    public static CypheringCurve getInstance() {
        logger.debug("Getting instance " + CypheringCurve.class.getName());
        if (instance == null) {
            instance = new CypheringCurve();
        }
        return instance;
    }

    public KeyPair generateKeyPairs() throws UnableToGenerateKeyPairs {
        KeyPair pair;
        try {
            KeyPairGenerator g = KeyPairGenerator.getInstance("ECDH", "BC");
            g.initialize(new ECGenParameterSpec("secp256k1"), new SecureRandom());
            pair = g.generateKeyPair();
        } catch (InvalidAlgorithmParameterException | NoSuchAlgorithmException | NoSuchProviderException ex) {
            throw new UnableToGenerateKeyPairs("Exception while generating key pairs", ex);
        }
        return pair;
    }

    public byte[] compressPublicKey(PublicKey key) {
        ECPublicKey eccPublicKeyEncoded = (ECPublicKey) key;
        return eccPublicKeyEncoded.getQ().getEncoded(true);
    }

    public byte[] compressPrivateKey(PrivateKey key) {
        ECPrivateKey eccPrivateKeyEncoded = (ECPrivateKey) key;
        return eccPrivateKeyEncoded.getD().toByteArray();
    }

    public PrivateKey decompressPrivateKey(byte[] data) throws UnableToDecompressKey {
        try {
            ECParameterSpec params = ECNamedCurveTable.getParameterSpec("secp256k1");
            ECPrivateKeySpec prvkey = new ECPrivateKeySpec(new BigInteger(data), params);
            KeyFactory kf = KeyFactory.getInstance("ECDH", "BC");
            return kf.generatePrivate(prvkey);
        } catch (NoSuchAlgorithmException | NoSuchProviderException | InvalidKeySpecException ex) {
            throw new UnableToDecompressKey("Unable to decompress Private key ", ex);
        }
    }

    public PublicKey decompressPublicKey(byte[] data) throws UnableToDecompressKey {
        try {
            ECParameterSpec params = ECNamedCurveTable.getParameterSpec("secp256k1");
            ECPublicKeySpec pubKey = new ECPublicKeySpec(
                    params.getCurve().decodePoint(data), params);
            KeyFactory kf = KeyFactory.getInstance("ECDH", "BC");
            return kf.generatePublic(pubKey);
        } catch (NoSuchAlgorithmException | NoSuchProviderException | InvalidKeySpecException ex) {
            throw new UnableToDecompressKey("Unable to decompress Public key ", ex);
        }
    }

    public byte[] generateSharedSecret(String serverEccPrivateKey, byte[] clientPublicKey) throws UnableToGenerateSharedSecret {

        try {
            final KeyAgreement keyAgreement = KeyAgreement.getInstance("ECDH");
            keyAgreement.init(decompressPrivateKey(Utils.toByteArray(serverEccPrivateKey)));
            keyAgreement.doPhase(decompressPublicKey(clientPublicKey), true);

            return keyAgreement.generateSecret();
        } catch (NoSuchAlgorithmException | UnableToDecompressKey | InvalidKeyException | IllegalStateException ex) {
            throw new UnableToGenerateSharedSecret("Error generating shared secret", ex);
        }

    }

    private byte[] shortenSecretKey(final byte[] longKey, Integer newSize) {

        final byte[] shortenedKey = new byte[newSize];
        System.arraycopy(longKey, 0, shortenedKey, 0, shortenedKey.length);
        return shortenedKey;

    }

    public byte[] generateAesKey(byte[] sharedSecret, byte[] apiKey) throws UnableToGenerateSharedSecret {
        try {
            PBEKeySpec pbeKeySpec = new PBEKeySpec(Utils.toHexString(sharedSecret).toCharArray(),
                    apiKey, 50, 256);
            SecretKeyFactory keyFactory = SecretKeyFactory
                    .getInstance("PBEWithSHA256And256BitAES-CBC-BC", "BC");
            SecretKeySpec secretKey = new SecretKeySpec(keyFactory.generateSecret(
                    pbeKeySpec).getEncoded(), "AES");
            byte[] key = secretKey.getEncoded();
            return key;
        } catch (NoSuchAlgorithmException | InvalidKeySpecException | NoSuchProviderException ex) {
            throw new UnableToGenerateSharedSecret("Error generating AES key", ex);
        }
    }

    public byte[] encryptData(byte[] data, byte[] aesKey, byte[] apiKey) throws UnableToEncryptDataException {

        try {
            SecretKey key = new SecretKeySpec(aesKey, 0, aesKey.length, "AES");
            Cipher cipher = Cipher.getInstance("AES/CTR/NoPadding", "BC");
            IvParameterSpec ivSpec = new IvParameterSpec(shortenSecretKey(apiKey, 16));
            cipher.init(Cipher.ENCRYPT_MODE, key, ivSpec);

            byte[] encrypted = cipher.doFinal(data);
            return encrypted;
        } catch (NoSuchAlgorithmException | NoSuchProviderException | NoSuchPaddingException | IllegalBlockSizeException | BadPaddingException | InvalidKeyException | InvalidAlgorithmParameterException ex) {
            throw new UnableToEncryptDataException("Error while cyphering data", ex);
        }
    }

    public byte[] decryptData(byte[] data, byte[] aesKey, byte[] apiKey) throws UnableToDecryptDataException {
        try {
            SecretKey key = new SecretKeySpec(aesKey, 0, aesKey.length, "AES");
            Cipher cipher = Cipher.getInstance("AES/CTR/NoPadding", "BC");
            IvParameterSpec ivSpec = new IvParameterSpec(shortenSecretKey(apiKey, 16));
            cipher.init(Cipher.DECRYPT_MODE, key, ivSpec);
            byte[] decrypted = cipher.doFinal(data);
            return decrypted;
        } catch (NoSuchAlgorithmException | InvalidAlgorithmParameterException | NoSuchProviderException | NoSuchPaddingException | IllegalBlockSizeException | BadPaddingException | InvalidKeyException ex) {
            throw new UnableToDecryptDataException("Error while cyphering data", ex);
        }
    }

    public byte[] getMD5Hash(byte[] byteArray) throws UnableToGenerateMD5HashException {

        MessageDigest md;
        try {
            md = MessageDigest.getInstance("MD5");
            byte[] hashApiKey = md.digest(byteArray);
            return hashApiKey;
        } catch (NoSuchAlgorithmException ex) {
            throw new UnableToGenerateMD5HashException("Error while generating MD5 hash ", ex);
        }

    }
}
