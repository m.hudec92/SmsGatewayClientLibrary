/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sk.stuba.fiit.dp.xhudecm1.smsgatewayclientlibrary.jsonobjects;

import com.google.gson.annotations.Expose;
import sk.stuba.fiit.dp.xhudecm1.smsgatewaysmshandler.smshandler.SmsSecurePDU;

/**
 *
 * @author martinhudec
 */
public class TemporaryMessage {

    @Expose
    private String data;
    @Expose
    private SmsSecurePDU.DestinationTypeEnum destinationType;
    @Expose
    private Object identifier;

    public TemporaryMessage(String data, SmsSecurePDU.DestinationTypeEnum destinationType, Object identifier) {
        this.data = data;
        this.destinationType = destinationType;
        this.identifier = identifier;
    }

    public TemporaryMessage() {
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public SmsSecurePDU.DestinationTypeEnum getDestinationType() {
        return destinationType;
    }

    public void setDestinationType(SmsSecurePDU.DestinationTypeEnum destinationType) {
        this.destinationType = destinationType;
    }

    public Object getIdentifier() {
        return identifier;
    }

    public void setIdentifier(Object identifier) {
        this.identifier = identifier;
    }

    @Override
    public String toString() {
        return "TemporaryMessage (data=" + data
                + ", destinationType=" + destinationType
                + ", identifier=" + identifier + ")";
    }

}
