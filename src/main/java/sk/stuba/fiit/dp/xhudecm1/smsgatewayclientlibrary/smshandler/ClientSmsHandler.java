/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sk.stuba.fiit.dp.xhudecm1.smsgatewayclientlibrary.smshandler;

import com.google.gson.FieldNamingPolicy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import sk.stuba.fiit.dp.xhudecm1.smsgatewayclientlibrary.SmsGatewayClient;
import sk.stuba.fiit.dp.xhudecm1.smsgatewayclientlibrary.bi.BusinessLogicHandler;
import sk.stuba.fiit.dp.xhudecm1.smsgatewayclientlibrary.compression.CompressionHandlerClient;
import sk.stuba.fiit.dp.xhudecm1.smsgatewayclientlibrary.exceptions.KeyExchangeNotFinishedYetException;
import sk.stuba.fiit.dp.xhudecm1.smsgatewayclientlibrary.exceptions.UnableToEncryptDataException;
import sk.stuba.fiit.dp.xhudecm1.smsgatewayclientlibrary.exceptions.UnableToLoadTemporaryMessageStorageJsonException;
import sk.stuba.fiit.dp.xhudecm1.smsgatewayclientlibrary.exceptions.UnableToWriteConfigurationFileException;
import sk.stuba.fiit.dp.xhudecm1.smsgatewayclientlibrary.jsonobjects.OptionProperties;
import sk.stuba.fiit.dp.xhudecm1.smsgatewayclientlibrary.jsonobjects.TemporaryMessage;
import sk.stuba.fiit.dp.xhudecm1.smsgatewayclientlibrary.jsonobjects.TemporaryMessageStorage;
import sk.stuba.fiit.dp.xhudecm1.smsgatewayclientlibrary.loadbalancer.LoadBalancer;
import sk.stuba.fiit.dp.xhudecm1.smsgatewayclientlibrary.message.SubmitMessage;
import sk.stuba.fiit.dp.xhudecm1.smsgatewayclientlibrary.utils.Utils;
import sk.stuba.fiit.dp.xhudecm1.smsgatewaydatacompression.CompressedData;

import sk.stuba.fiit.dp.xhudecm1.smsgatewaysmshandler.smshandler.DeliverSmsPDU;
import sk.stuba.fiit.dp.xhudecm1.smsgatewaysmshandler.smshandler.DeliveryStatusSmsPDU;
import sk.stuba.fiit.dp.xhudecm1.smsgatewaysmshandler.smshandler.ModemInstance;
import sk.stuba.fiit.dp.xhudecm1.smsgatewaysmshandler.smshandler.SmsHandler;
import sk.stuba.fiit.dp.xhudecm1.smsgatewaysmshandler.smshandler.SmsSecurePDU;
import sk.stuba.fiit.dp.xhudecm1.smsgatewaysmshandler.smshandler.SmsSecurePDUData;
import sk.stuba.fiit.dp.xhudecm1.smsgatewaysmshandler.smshandler.SubmitSmsPDU;
import sk.stuba.fiit.dp.xhudecm1.smsgatewaysmshandler.smshandler.exceptions.UknownPduConentType;
import sk.stuba.fiit.dp.xhudecm1.smsgatewaysmshandler.smshandler.exceptions.UnableToLoadModemConfiguration;
import sk.stuba.fiit.dp.xhudemc1.serialgsmmodemcommunicator.exceptions.SerialGsmModemCommunicatorException;

/**
 *
 * @author martinhudec
 */
public class ClientSmsHandler extends SmsHandler implements ClientSmsHandlerInterface {

    final static org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(ClientSmsHandler.class);
    private LoadBalancer loadBalancer = null;
    private BusinessLogicHandler biHandler;
    private CompressionHandlerClient compressionHandler;
    private List<MessageSentCallback> messageSentCallbackList = new ArrayList<>();

    public ClientSmsHandler() throws UnableToWriteConfigurationFileException, SerialGsmModemCommunicatorException, UnableToLoadModemConfiguration {
        super();
        logger.debug("CLIENT SMS HANDLER CONSTRUCTOR");
        loadBalancer = new LoadBalancer(modemList);
        logger.debug("loadBalancer created");

        init();

    }

    private void init() throws UnableToWriteConfigurationFileException {
        logger.debug("Initializing client sms handler");
        this.biHandler = new BusinessLogicHandler(this);
        this.compressionHandler = CompressionHandlerClient.getInstance();
    }

    @Override
    protected void specifyModems() throws SerialGsmModemCommunicatorException {
        logger.debug("SPECIFING MODEMS");
        for (OptionProperties.Modems modem : SmsGatewayClient.OPTION_PROPERTIES.getModems()) {
            ModemInstance newModemInstance = new ModemInstance(modem.getSerialPort(), "SM", modem.getSimPin(), modem.getOperator());
            modemList.add(newModemInstance);
        }
    }

    @Override
    protected ModemInstance getModem(SubmitSmsPDU pdu) {
        return loadBalancer.getAppropriateModem(pdu);
    }

    @Override
    protected void processReceivedMessage(DeliverSmsPDU receivedMessage) {
        try {
            biHandler.processReceivedSMSMessage(receivedMessage);
        } catch (UknownPduConentType ex) {
            Logger.getLogger(ClientSmsHandler.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public void registerMessageReceivedCallback(MessageReceivedCallback callback) {
        biHandler.addMessageReceivedCallback(callback);
    }

    @Override
    public void unRegisterMessageReceivedCallback(MessageReceivedCallback callback) {
        biHandler.removeMessageReceivedCallback(callback);
    }

    @Override
    public void sendSecureMessageToDestination(byte[] data, SmsSecurePDU.DestinationTypeEnum destinationType, Object identifier) throws UnableToLoadTemporaryMessageStorageJsonException, KeyExchangeNotFinishedYetException, UnableToEncryptDataException {
        // najskor je potrebne prijate data skomprimovat 

        if (biHandler.isAbleToSendSecureMessage()) {
            try {
                CompressedData compressedData = compressionHandler.compressData(data, null);
                logger.debug("Compressed Data " + new String(compressedData.getCompressed()));
                SmsSecurePDU securePDU = new SmsSecurePDUData(
                        biHandler.getNextPduSeqNumber(),
                        SmsSecurePDU.ContentTypeEnum.DATA,
                        SmsSecurePDU.CypherActiveEnum.ON,
                        compressedData.getCompressionType(),
                        destinationType,
                        biHandler.encryptData(compressedData.getCompressed()));

                SubmitSmsPDU submitPDU = new SubmitSmsPDU(
                        SmsGatewayClient.OPTION_PROPERTIES.getGatewayServerNumber(),
                        securePDU);
                SubmitMessage submitMessage = new SubmitMessage(data, securePDU, submitPDU, identifier);
                sendMessage(submitPDU);

                for (MessageSentCallback callback : messageSentCallbackList) {
                    callback.messageSent(submitMessage);
                }
            } catch (IOException ex) {
                Logger.getLogger(ClientSmsHandler.class.getName()).log(Level.SEVERE, null, ex);
            }
        } else {
            logger.debug("Secure logic not ready yet storing messages to json ");
            TemporaryMessageStorage tempMessages;
            try {
                try {
                    tempMessages = loadTempMessages();
                } catch (FileNotFoundException ex) {
                    logger.warn("no Temp file exists yet creating one ", ex);
                    tempMessages = new TemporaryMessageStorage(new ArrayList<>());
                }
                tempMessages.getMessageList().add(new TemporaryMessage(Utils.toHexString(data), destinationType, identifier));
                writeTempMessages(tempMessages);
            } catch (IOException ex) {
                throw new UnableToLoadTemporaryMessageStorageJsonException("Exception while loading temporary storage json", ex);
            }
        }
    }

    @Override
    public void sendSecureMessage(byte[] data, Object identifier) throws UnableToLoadTemporaryMessageStorageJsonException, KeyExchangeNotFinishedYetException, UnableToEncryptDataException {

        if (biHandler.isAbleToSendSecureMessage()) {
            try {
                CompressedData compressedData = compressionHandler.compressData(data, null);

                logger.debug("Compressed Data " + new String(compressedData.getCompressed()));
                SmsSecurePDU securePDU = new SmsSecurePDUData(
                        biHandler.getNextPduSeqNumber(),
                        SmsSecurePDU.ContentTypeEnum.DATA,
                        SmsSecurePDU.CypherActiveEnum.ON,
                        compressedData.getCompressionType(),
                        SmsSecurePDU.DestinationTypeEnum.DEFAULT,
                        biHandler.encryptData(compressedData.getCompressed()));

                SubmitSmsPDU submitPDU = new SubmitSmsPDU(
                        SmsGatewayClient.OPTION_PROPERTIES.getGatewayServerNumber(),
                        securePDU);
                sendMessage(submitPDU);
                SubmitMessage submitMessage = new SubmitMessage(data, securePDU, submitPDU, identifier);
                for (MessageSentCallback callback : messageSentCallbackList) {
                    callback.messageSent(submitMessage);
                }
            } catch (IOException ex) {
                Logger.getLogger(ClientSmsHandler.class.getName()).log(Level.SEVERE, null, ex);
            }
        } else {
            logger.debug("Secure logic not ready yet storing messages to json ");
            TemporaryMessageStorage tempMessages;
            try {
                try {
                    tempMessages = loadTempMessages();
                } catch (FileNotFoundException ex) {
                    logger.warn("no Temp file exists yet creating one ", ex);
                    tempMessages = new TemporaryMessageStorage(new ArrayList<>());
                }
                tempMessages.getMessageList().add(new TemporaryMessage(Utils.toHexString(data), SmsSecurePDU.DestinationTypeEnum.DEFAULT, identifier));
                writeTempMessages(tempMessages);
            } catch (IOException ex) {
                throw new UnableToLoadTemporaryMessageStorageJsonException("Exception while loading temporary storage json", ex);
            }
        }

    }

    @Override
    public void sendCustomSpecifiedMessage(byte[] data, SmsSecurePDU.CompressionTypeEnum compressionType, SmsSecurePDU.CypherActiveEnum cypherActive, Object identifier) throws UnableToLoadTemporaryMessageStorageJsonException, KeyExchangeNotFinishedYetException, UnableToEncryptDataException {

        if (biHandler.isAbleToSendSecureMessage()) {
            try {
                CompressedData compressedData = null;
                if (compressionType == null) {
                    compressedData = compressionHandler.compressData(data, null);
                } else {
                    compressedData = compressionHandler.compressDataSpecific(data, null, compressionType);
                }
                logger.debug("Compressed Data " + new String(compressedData.getCompressed()));
                byte[] encrypted = compressedData.getCompressed();
                if (cypherActive.equals(SmsSecurePDU.CypherActiveEnum.ON)) {
                    encrypted = biHandler.encryptData(compressedData.getCompressed());
                }
                SmsSecurePDU securePDU = new SmsSecurePDUData(
                        biHandler.getNextPduSeqNumber(),
                        SmsSecurePDU.ContentTypeEnum.DATA,
                        cypherActive,
                        compressedData.getCompressionType(),
                        SmsSecurePDU.DestinationTypeEnum.DEFAULT,
                        encrypted);

                SubmitSmsPDU submitPDU = new SubmitSmsPDU(
                        SmsGatewayClient.OPTION_PROPERTIES.getGatewayServerNumber(),
                        securePDU);
                sendMessage(submitPDU);
                SubmitMessage submitMessage = new SubmitMessage(data, securePDU, submitPDU, identifier);
                for (MessageSentCallback callback : messageSentCallbackList) {
                    callback.messageSent(submitMessage);
                }

            } catch (IOException ex) {
                Logger.getLogger(ClientSmsHandler.class.getName()).log(Level.SEVERE, null, ex);
            }
        } else {
            logger.debug("Secure logic not ready yet storing messages to json ");
            TemporaryMessageStorage tempMessages;
            try {
                try {
                    tempMessages = loadTempMessages();
                } catch (FileNotFoundException ex) {
                    logger.warn("no Temp file exists yet creating one ", ex);
                    tempMessages = new TemporaryMessageStorage(new ArrayList<>());
                }
                tempMessages.getMessageList().add(new TemporaryMessage(Utils.toHexString(data), SmsSecurePDU.DestinationTypeEnum.DEFAULT, identifier));
                writeTempMessages(tempMessages);
            } catch (IOException ex) {
                throw new UnableToLoadTemporaryMessageStorageJsonException("Exception while loading temporary storage json", ex);
            }
        }

    }

    @Override
    public void registerMessageStatusDeliveryCallback(DeliveryStatusMessageReceivedCallback callback) {
        biHandler.addDeliveryStatusMessageReceivedCallback(callback);
    }

    @Override
    protected void processReceivedDeliveryStatusMessage(DeliveryStatusSmsPDU deliveryStatusMessage) {
        biHandler.processReceivedDeliveryStatusMessage(deliveryStatusMessage);
    }

    public void sendTemporarilyStoredMessages() {
        logger.debug("SECURITY LOGIC available, sending stored messages");
        TemporaryMessageStorage tempMessages;
        try {
            tempMessages = loadTempMessages();
            for (Iterator<TemporaryMessage> it = tempMessages.getMessageList().iterator(); it.hasNext();) {
                TemporaryMessage temporaryMessage = it.next();
                logger.debug("sending data " + temporaryMessage.getData() + "\n" + new String(Utils.toByteArray(temporaryMessage.getData())));
                this.sendSecureMessageToDestination(Utils.toByteArray(temporaryMessage.getData()), temporaryMessage.getDestinationType(), temporaryMessage.getIdentifier());
                it.remove();
            }
            writeTempMessages(tempMessages);
        } catch (UnableToLoadTemporaryMessageStorageJsonException | IOException | KeyExchangeNotFinishedYetException | UnableToEncryptDataException ex) {
            logger.error("Error while  sending temporarily stored messages ", ex);
        }
    }

    public void sendMessageUnsecureTest(byte[] data, String number) {
        SubmitSmsPDU submitPdu = new SubmitSmsPDU(number, data);
        sendMessage(submitPdu);

        SubmitMessage submitMessage = new SubmitMessage(data, null, submitPdu);
        for (MessageSentCallback callback : messageSentCallbackList) {
            callback.messageSent(submitMessage);
        }
    }

    private TemporaryMessageStorage loadTempMessages() throws FileNotFoundException {
        BufferedReader reader = null;
        Gson gson = new GsonBuilder()
                .disableHtmlEscaping()
                .setFieldNamingPolicy(FieldNamingPolicy.UPPER_CAMEL_CASE)
                .setPrettyPrinting()
                .serializeNulls()
                .create();
        reader = new BufferedReader(new FileReader(SmsGatewayClient.OPTION_PROPERTIES.getTemporaryMessageStoragePath()));
        TemporaryMessageStorage data = gson.fromJson(reader, TemporaryMessageStorage.class
        );
        return data;
    }

    private void writeTempMessages(TemporaryMessageStorage tempMessageStorage) throws IOException {
        Gson gson = new GsonBuilder()
                .disableHtmlEscaping()
                .setFieldNamingPolicy(FieldNamingPolicy.UPPER_CAMEL_CASE)
                .setPrettyPrinting()
                .serializeNulls()
                .create();
        File configFile = new File(SmsGatewayClient.OPTION_PROPERTIES.getTemporaryMessageStoragePath());
        configFile.getParentFile().mkdirs();
        Writer writer;
        writer = new FileWriter(configFile);
        writer.write(gson.toJson(tempMessageStorage));
        writer.close();
    }

    @Override
    public void registerMessageSentCallback(MessageSentCallback callback) {
        this.messageSentCallbackList.add(callback);
    }
}
