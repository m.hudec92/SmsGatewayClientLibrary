/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sk.stuba.fiit.dp.xhudecm1.smsgatewayclientlibrary.exceptions;

/**
 *
 * @author martinhudec
 */
public class UnableToWriteConfigurationFileException extends Exception {

    public UnableToWriteConfigurationFileException(String string) {
        super(string);
    }

    public UnableToWriteConfigurationFileException(String string, Throwable th) {
        super(string, th);
    }
}
