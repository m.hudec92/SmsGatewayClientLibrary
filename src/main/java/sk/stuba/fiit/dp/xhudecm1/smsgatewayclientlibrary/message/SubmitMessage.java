/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sk.stuba.fiit.dp.xhudecm1.smsgatewayclientlibrary.message;

import java.util.UUID;
import sk.stuba.fiit.dp.xhudecm1.smsgatewaysmshandler.smshandler.SmsSecurePDU;
import sk.stuba.fiit.dp.xhudecm1.smsgatewaysmshandler.smshandler.SubmitSmsPDU;

/**
 *
 * @author martinhudec
 */
public class SubmitMessage {

    private byte[] data;

    private SmsSecurePDU securePdu;
    private SubmitSmsPDU submitPdu;
    private Object identifier;

    public SubmitMessage(byte[] data, SmsSecurePDU securePdu, SubmitSmsPDU submitPdu, Object identifier) {
        this.data = data;
        this.securePdu = securePdu;
        this.submitPdu = submitPdu;
        this.identifier = identifier;
    }

    public SubmitMessage(byte[] data, SmsSecurePDU securePdu, SubmitSmsPDU submitPdu) {
        this.data = data;
        this.securePdu = securePdu;
        this.submitPdu = submitPdu;
        this.identifier = identifier;
    }

    public byte[] getData() {
        return data;
    }

    public SmsSecurePDU getSecurePdu() {
        return securePdu;
    }

    public SubmitSmsPDU getSubmitPdu() {
        return submitPdu;
    }

    public Object getIdentifier() {
        return identifier;
    }

    public void setIdentifier(Object identifier) {
        this.identifier = identifier;
    }

}
