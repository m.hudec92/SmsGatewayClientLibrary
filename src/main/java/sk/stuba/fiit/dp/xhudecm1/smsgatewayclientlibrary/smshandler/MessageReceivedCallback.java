/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sk.stuba.fiit.dp.xhudecm1.smsgatewayclientlibrary.smshandler;

import java.util.UUID;
import sk.stuba.fiit.dp.xhudecm1.smsgatewayclientlibrary.message.ReceivedMessage;
import sk.stuba.fiit.dp.xhudecm1.smsgatewaysmshandler.smshandler.DeliverSmsPDU;
import sk.stuba.fiit.dp.xhudecm1.smsgatewaysmshandler.smshandler.ModemInstance;

/**
 *
 * @author martinhudec
 */
public interface MessageReceivedCallback {

    public void messageReceived(ReceivedMessage receivedMessage);

}
