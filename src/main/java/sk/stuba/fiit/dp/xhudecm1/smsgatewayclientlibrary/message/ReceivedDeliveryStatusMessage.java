/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sk.stuba.fiit.dp.xhudecm1.smsgatewayclientlibrary.message;

import java.util.UUID;
import sk.stuba.fiit.dp.xhudecm1.serialgsmmodemcommunicator.sms.SmsPDUStatusReport;
import sk.stuba.fiit.dp.xhudecm1.smsgatewaysmshandler.smshandler.SmsSecurePDU;

/**
 *
 * @author martinhudec
 */
public class ReceivedDeliveryStatusMessage {

    private final UUID messageUUID;
    private final Integer referenceNumber;
    private final String originatorNumber;
    private final SmsPDUStatusReport.DeliveryStatusType deliveryStatus;

    public ReceivedDeliveryStatusMessage(UUID messageUUID, Integer referenceNumber, String originatorNumber, SmsPDUStatusReport.DeliveryStatusType deliveryStatus) {
        this.messageUUID = messageUUID;
        this.referenceNumber = referenceNumber;
        this.originatorNumber = originatorNumber;
        this.deliveryStatus = deliveryStatus;
    }

    public Integer getReferenceNumber() {
        return referenceNumber;
    }

    public String getOriginatorNumber() {
        return originatorNumber;
    }

    public UUID getMessageUUID() {
        return messageUUID;
    }

    public SmsPDUStatusReport.DeliveryStatusType getDeliveryStatus() {
        return deliveryStatus;
    }

}
