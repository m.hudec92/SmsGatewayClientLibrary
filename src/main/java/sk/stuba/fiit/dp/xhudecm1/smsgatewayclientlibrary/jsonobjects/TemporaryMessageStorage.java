/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sk.stuba.fiit.dp.xhudecm1.smsgatewayclientlibrary.jsonobjects;

import com.google.gson.annotations.Expose;
import java.util.List;
import sk.stuba.fiit.dp.xhudecm1.smsgatewaysmshandler.smshandler.SmsSecurePDU;

/**
 *
 * @author martinhudec
 */
public class TemporaryMessageStorage {

    @Expose
    private List<TemporaryMessage> messageList;

    public TemporaryMessageStorage(List<TemporaryMessage> messageList) {
        this.messageList = messageList;
    }

    public TemporaryMessageStorage() {
    }

    public List<TemporaryMessage> getMessageList() {
        return messageList;
    }

    public void setMessageList(List<TemporaryMessage> messageList) {
        this.messageList = messageList;
    }

    @Override
    public String toString() {
        return "TemporaryMessageStorage [messageList=" + messageList.toString() + "]";
    }
}
