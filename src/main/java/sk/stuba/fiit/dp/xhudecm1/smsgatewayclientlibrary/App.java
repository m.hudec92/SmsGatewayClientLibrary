/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sk.stuba.fiit.dp.xhudecm1.smsgatewayclientlibrary;

import java.io.FileNotFoundException;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;
import sk.stuba.fiit.dp.xhudecm1.smsgatewayclientlibrary.exceptions.KeyExchangeNotFinishedYetException;
import sk.stuba.fiit.dp.xhudecm1.smsgatewayclientlibrary.exceptions.UnableToEncryptDataException;
import sk.stuba.fiit.dp.xhudecm1.smsgatewayclientlibrary.exceptions.UnableToLoadTemporaryMessageStorageJsonException;
import sk.stuba.fiit.dp.xhudecm1.smsgatewayclientlibrary.exceptions.UnableToWriteConfigurationFileException;
import sk.stuba.fiit.dp.xhudecm1.smsgatewayclientlibrary.message.ReceivedDeliveryStatusMessage;
import sk.stuba.fiit.dp.xhudecm1.smsgatewayclientlibrary.message.ReceivedMessage;
import sk.stuba.fiit.dp.xhudecm1.smsgatewayclientlibrary.message.SubmitMessage;
import sk.stuba.fiit.dp.xhudecm1.smsgatewayclientlibrary.smshandler.MessageSentCallback;
import sk.stuba.fiit.dp.xhudecm1.smsgatewaysmshandler.smshandler.MessageSentResultCallback;
import sk.stuba.fiit.dp.xhudecm1.smsgatewaysmshandler.smshandler.ModemInstance;
import sk.stuba.fiit.dp.xhudecm1.smsgatewaysmshandler.smshandler.SmsSecurePDU;
import sk.stuba.fiit.dp.xhudecm1.smsgatewaysmshandler.smshandler.exceptions.UnableToLoadModemConfiguration;
import sk.stuba.fiit.dp.xhudemc1.serialgsmmodemcommunicator.exceptions.SerialGsmModemCommunicatorException;

/**
 *
 * @author martinhudec
 */
public class App {

    /**
     * @param args the command line arguments
     */
    private final static org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(App.class);

    public static void main(String[] args) {
        try {
            SmsGatewayClient smsClient;

            smsClient = new SmsGatewayClient("/opt/smsGatewayClientLibraryData/configuration/option.properties");

            smsClient.getSmsHandler().registerMessageReceivedCallback((ReceivedMessage receivedMessage) -> {
                logger.debug("Received new message " + receivedMessage.getMessageUUID() + " RECEIVED MESSAGE " + new String(receivedMessage.getData()));
            });

            smsClient.getSmsHandler().registerMessageStatusDeliveryCallback((ReceivedDeliveryStatusMessage receivedMessage) -> {
                logger.debug("Received new delivery status message with ref  " + receivedMessage.getReferenceNumber() + "from number " + receivedMessage.getOriginatorNumber() + " STATUS: " + receivedMessage.getDeliveryStatus());
                logger.info("[" + receivedMessage.getOriginatorNumber() + " ][" + receivedMessage.getDeliveryStatus() + "][" + receivedMessage.getReferenceNumber() + "] delivered " + System.nanoTime());
            });

            smsClient.getSmsHandler().registerMessageSentResultCallback((UUID messageUuid, Integer referenceNumber, MessageSentResultCallback.MessageSentResultEnum result, ModemInstance modem) -> {
                logger.debug("Message sent result " + result + " uuid " + messageUuid + " ref num " + referenceNumber);
                logger.info("[" + messageUuid + " ][" + result + "][" + referenceNumber + "] result " + System.nanoTime());
            });

            smsClient.getSmsHandler().registerMessageSentCallback((SubmitMessage submitMessage) -> {

                logger.info("[" + submitMessage.getIdentifier() + "]"
                        + "[" + submitMessage.getSubmitPdu().getMessageUUID() + " ]");
            });

            smsClient.getSmsHandler().registerMessageSentCallback((SubmitMessage submitMessage) -> {

                logger.debug("submittedMessage uuid " + submitMessage.getSubmitPdu().getMessageUUID() + " " + submitMessage.getIdentifier());
                switch ((String) submitMessage.getIdentifier()) {
                    case "jsonON":
                        logger.info("[jsonON][OFF][NONE][" + submitMessage.getSubmitPdu().getMessageUUID() + " ] sent " + System.nanoTime());
                        break;
                    case "textEngON":
                        logger.info("[textEngON][OFF][NONE][" + submitMessage.getSubmitPdu().getMessageUUID() + " ] sent " + System.nanoTime());
                        break;
                    case "randomCharsON":
                        logger.info("[randomCharsON][OFF][NONE][" + submitMessage.getSubmitPdu().getMessageUUID() + " ] sent " + System.nanoTime());
                        break;
                    case "slovakTextON":
                        logger.info("[slovakTextON][OFF][NONE][" + submitMessage.getSubmitPdu().getMessageUUID() + " ] sent " + System.nanoTime());
                        break;
                }

            });

            String textEng = "Copies an array from the specified source array, beginning at the specified position, to the specified position of the ";
            String randomChars = "409jdfa[sd]f'34';bv\\]/ xvbn.z/B\"peo5'42543/52|/XV\"b/XVB][xvb?2@%:@\\4?VB?XCVB}u1423\\'X|Vb']";
            String slovakText = "Povedal to na tlačovej konferencii v reakcii na staršie vyhlásenia prezidenta ktorý presadzoval, aby vláda zostala v";
            String json = "{\n"
                    + "   \"Modems\":[\n"
                    + "      {\n"
                    + "         \"Name\":\"Huawei E367\",\n"
                    + "         \"Manufacturer\":\"Huawei\",\n"
                    + "         \"SerialPort\":\"/dev/ttyUSB2\",\n"
                    + "      }\n"
                    + "   ]\n"
                    + "}";

            byte[] data = "dataToBeTransmitted".getBytes();
            Object myIdentifier = "myidentifier";
            smsClient.getSmsHandler().sendSecureMessageToDestination(data, SmsSecurePDU.DestinationTypeEnum.EMAIL, myIdentifier);

            logger.info("[textEngON][OFF][NONE] start " + System.nanoTime());
            smsClient.getSmsHandler().sendCustomSpecifiedMessage(textEng.getBytes(), null, SmsSecurePDU.CypherActiveEnum.ON, "textEngON");
            Thread.sleep(10000);
            //smsClient.getSmsHandler().sendMessageUnsecureTest("TEST".getBytes(), "+421908632636");
            logger.info("[jsonON][OFF][NONE] start " + System.nanoTime());
            smsClient.getSmsHandler().sendCustomSpecifiedMessage(json.getBytes(), SmsSecurePDU.CompressionTypeEnum.NONE, SmsSecurePDU.CypherActiveEnum.ON, "jsonON");

            Thread.sleep(10000);
            logger.info("[textEngON][OFF][NONE] start " + System.nanoTime());
            smsClient.getSmsHandler().sendCustomSpecifiedMessage(textEng.getBytes(), null, SmsSecurePDU.CypherActiveEnum.ON, "textEngON");

            Thread.sleep(10000);
            logger.info("[randomCharsON][OFF][NONE] start " + System.nanoTime());
            smsClient.getSmsHandler().sendCustomSpecifiedMessage(randomChars.getBytes(), null, SmsSecurePDU.CypherActiveEnum.ON, "randomCharsON");
            Thread.sleep(10000);
            logger.info("[slovakTextON][OFF][NONE] start " + System.nanoTime());
            smsClient.getSmsHandler().sendCustomSpecifiedMessage(slovakText.getBytes(), null, SmsSecurePDU.CypherActiveEnum.ON, "slovakTextON");

        } catch (UnableToWriteConfigurationFileException | KeyExchangeNotFinishedYetException | SerialGsmModemCommunicatorException | UnableToEncryptDataException | UnableToLoadTemporaryMessageStorageJsonException | UnableToLoadModemConfiguration ex) {
            Logger.getLogger(App.class.getName()).log(Level.SEVERE, null, ex);
        } catch (InterruptedException ex) {
            Logger.getLogger(App.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

}
