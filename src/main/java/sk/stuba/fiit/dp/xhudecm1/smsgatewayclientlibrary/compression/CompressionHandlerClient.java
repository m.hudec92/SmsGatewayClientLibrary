/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sk.stuba.fiit.dp.xhudecm1.smsgatewayclientlibrary.compression;

import com.google.gson.FieldNamingPolicy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import sk.stuba.fiit.dp.xhudecm1.smsgatewayclientlibrary.SmsGatewayClient;
import sk.stuba.fiit.dp.xhudecm1.smsgatewayclientlibrary.bi.BusinessLogicHandler;
import sk.stuba.fiit.dp.xhudecm1.smsgatewayclientlibrary.jsonobjects.OptionProperties;
import sk.stuba.fiit.dp.xhudecm1.smsgatewaydatacompression.CompressedData;
import sk.stuba.fiit.dp.xhudecm1.smsgatewaydatacompression.CompressionHandler;
import sk.stuba.fiit.dp.xhudecm1.smsgatewaydatacompression.jsonObjects.CustomAlphabet;
import sk.stuba.fiit.dp.xhudecm1.smsgatewaysmshandler.smshandler.SmsSecurePDU;

/**
 *
 * @author martinhudec
 */
public class CompressionHandlerClient extends CompressionHandler {

    private static CompressionHandlerClient instance;
    final static org.apache.log4j.Logger LOG = org.apache.log4j.Logger.getLogger(CompressionHandlerClient.class);
    private CustomAlphabet customAlphabet;
    private CompressionHandlerClient() {
        try {
            customAlphabet = loadCustomAlphabet();
            LOG.debug("setting custom alphabet");
        } catch (FileNotFoundException ex) {
            LOG.error("Error while loading custom alphabet, it will not be used during compression", ex);
        }
    }

    public static CompressionHandlerClient getInstance() {
        if (instance == null) {
            instance = new CompressionHandlerClient();
        }

        return instance;
    }

    @Override
    public CompressedData compressData(byte[] data, CustomAlphabet customAlphabet) throws IOException {
        return super.compressData(data, this.customAlphabet); //To change body of generated methods, choose Tools | Templates.
    }
    
    
    public CompressedData compressDataSpecific(byte[] data, CustomAlphabet customAlphabet, SmsSecurePDU.CompressionTypeEnum compressionType) throws IOException {
        return super.compressDataSpecific(data, this.customAlphabet, compressionType); //To change body of generated methods, choose Tools | Templates.
    }
    
    @Override
    public byte[] decompressData(byte[] data, SmsSecurePDU.CompressionTypeEnum compressionType, CustomAlphabet customAlphabet) throws IOException {
        return super.decompressData(data, compressionType, this.customAlphabet); //To change body of generated methods, choose Tools | Templates.
    }
    
    private CustomAlphabet loadCustomAlphabet() throws FileNotFoundException {
        if (SmsGatewayClient.OPTION_PROPERTIES.getCustomAlphabetPath() == null){
            return null;
        }
        Gson gson = new GsonBuilder()
                .disableHtmlEscaping()
                .setFieldNamingPolicy(FieldNamingPolicy.UPPER_CAMEL_CASE)
                .setPrettyPrinting()
                .serializeNulls()
                .create();
        BufferedReader reader = new BufferedReader(new FileReader(SmsGatewayClient.OPTION_PROPERTIES.getCustomAlphabetPath()));
        CustomAlphabet data = gson.fromJson(reader, CustomAlphabet.class);
        return data;
    }
}
