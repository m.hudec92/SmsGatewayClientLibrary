/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sk.stuba.fiit.dp.xhudecm1.smsgatewayclientlibrary.smshandler;

import java.io.FileNotFoundException;
import sk.stuba.fiit.dp.xhudecm1.smsgatewayclientlibrary.exceptions.KeyExchangeNotFinishedYetException;
import sk.stuba.fiit.dp.xhudecm1.smsgatewayclientlibrary.exceptions.UnableToEncryptDataException;
import sk.stuba.fiit.dp.xhudecm1.smsgatewayclientlibrary.exceptions.UnableToLoadTemporaryMessageStorageJsonException;
import sk.stuba.fiit.dp.xhudecm1.smsgatewayclientlibrary.message.SubmitMessage;
import sk.stuba.fiit.dp.xhudecm1.smsgatewaysmshandler.smshandler.MessageSentResultCallback;
import sk.stuba.fiit.dp.xhudecm1.smsgatewaysmshandler.smshandler.SmsSecurePDU;
import sk.stuba.fiit.dp.xhudecm1.smsgatewaysmshandler.smshandler.SubmitSmsPDU;

/**
 *
 * @author martinhudec
 */
public interface ClientSmsHandlerInterface {

    public void sendSecureMessageToDestination(byte[] data, SmsSecurePDU.DestinationTypeEnum destinationType, Object identifier) throws UnableToLoadTemporaryMessageStorageJsonException, KeyExchangeNotFinishedYetException, UnableToEncryptDataException;

    public void sendSecureMessage(byte[] data, Object identifier) throws UnableToLoadTemporaryMessageStorageJsonException, KeyExchangeNotFinishedYetException, UnableToEncryptDataException;

    public void sendMessageUnsecureTest(byte[] data, String number);

    public void sendCustomSpecifiedMessage(byte[] data, SmsSecurePDU.CompressionTypeEnum compressionType, SmsSecurePDU.CypherActiveEnum cypherActive, Object identifier) throws UnableToLoadTemporaryMessageStorageJsonException, KeyExchangeNotFinishedYetException, UnableToEncryptDataException;
    
    public void registerMessageSentResultCallback(MessageSentResultCallback callback);

    public void registerMessageReceivedCallback(MessageReceivedCallback callback);

    public void unRegisterMessageReceivedCallback(MessageReceivedCallback callback);

    public void registerMessageSentCallback(MessageSentCallback callbackF);

    public void registerMessageStatusDeliveryCallback(DeliveryStatusMessageReceivedCallback callback);
}
