/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sk.stuba.fiit.dp.xhudecm1.smsgatewayclientlibrary.exceptions;

/**
 *
 * @author martinhudec
 */
public class UnableToProcessReceivedTechnicalMessageException extends Exception{

    public UnableToProcessReceivedTechnicalMessageException(String string) {
        super(string);
    }

    public UnableToProcessReceivedTechnicalMessageException(String string, Throwable th) {
        super(string, th);
    }
}
