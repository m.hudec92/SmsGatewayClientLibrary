/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sk.stuba.fiit.dp.xhudecm1.smsgatewayclientlibrary.smshandler;

import sk.stuba.fiit.dp.xhudecm1.smsgatewayclientlibrary.message.ReceivedDeliveryStatusMessage;
import sk.stuba.fiit.dp.xhudecm1.smsgatewayclientlibrary.message.ReceivedMessage;

/**
 *
 * @author martinhudec
 */
public interface DeliveryStatusMessageReceivedCallback {

    public void messageDeliveryStatusReceived(ReceivedDeliveryStatusMessage receivedMessage);

}
